# Virtual machine parameters #
## Users ##

    login: user
     pass: zse4rfvxdr5tgb
    login: root
     pass: ExtensapowerbuttoN

## SSH ##
### Local susu network: ###
    10.0.4.8:222  
### External network: ###
    37.75.248.236:2220

## Git ##
    git_pass: passphrase

## SimGrid location ##
### SimGrid home dir: ###
    /opt/simgrid

### SimGrid java home dir:###
    /opt/simgrid/java

## Project home dir ##
    /home/user/Workspace/divtb-model

## Build ##
Run from task root (*Don't forget pull latest stable version of sources from git*)

    mvn clean install assembly:single

## Run ##
Run from task root

    java -jar ./target/divtb-model-0.0.1-jar-with-dependencies.jar ./cfg/default/platform.xml ./cfg/default/application.xml

# Short SimGrid tutorial #
## Install SimGrid dependencies ##
### From repo: ###
    gcc gcc-c++ make cmake automake autoconf graphviz graphviz-devel f2c flex pcre pcre-devel perl-libwww-perl
*__WARNING:__ Full list of dependencies may depend on your linux distro.*
### From other sources: ###
1. [flexml](http://sourceforge.net/projects/flexml/ "flexml home page")
2. jdk1.5 *or newer*
3. jni
4. libgcj10 *or newer*

## Prepare environment variables ##
1. $JAVA_HOME=/your/java/home/dir
2. $JAVA_INCLUDE_PATH=$JAVA_HOME/include
3. $JAVA_INCLUDE_PATH2=$JAVA_HOME/include/linux
4. $SIMGRID_ROOT=/simgrid/home/dir
5. $SIMGRID_JAVA_ROOT=$SIMGRID_ROOT/java
6. $LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$SIMGRID_ROOT/lib:$SIMGRID_JAVA_ROOT

## Install SimGrid ##
    |> git clone git://scm.gforge.inria.fr/simgrid/simgrid.git simgrid
    |> cd ./simgrid
    |> cmake -DCMAKE_INSTALL_PREFIX=$SIMGRID_ROOT -Denable_java=on ./
    |> make
    |> make install
*__WARNING:__ SimGrid has task with __SMPI__ on __OpenSUSE12.2__. To use SimGrid on OpenSUSE12.2 you should disable SIMPI in __cmake__. Use parametr __-Denable_smpi=off__ to disable SMPI.*

## Configure  __Maven__##
Add SimGrid to local maven repo:

    mvn install:install-file -Dfile=$SIMGRID_JAVA_ROOT/simgrid.jar -DgroupId=org.simgrid -DartifactId=simgrid -Dversion=<simgird-version> -Dpackaging=jar

## Prepare task to use SimGrid ##
Add dependency to pom.xml file

    <dependency>
        <groupId>org.simgrid</groupId>
        <artifactId>simgrid</artifactId>
        <version><simgird-version></version>
    </dependency>