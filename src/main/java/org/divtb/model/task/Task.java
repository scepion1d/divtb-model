package org.divtb.model.task;

import org.divtb.model.nodes.Command;
import org.divtb.model.xml.TaskXMLDocument;

/**
 * Класс описывающий задание
 */
public class Task extends org.simgrid.msg.Task {

    /**
     * Параметры выполнения задания
     */
    private TaskXMLDocument parameters;

    /**
     * Статус выполнения задания
     */
    private TaskStatus taskStatus;

    /**
     * Действие которое требуется выполнить по заданию
     */
    private Command command;

    /**
     * Размер задания
     */
    private Double dataSize;

    /**
     * Order of the task execution
     */
    private Object data;

    /**
     * Initialize task like simple command to application
     *
     * @param command Command
     */
    public Task(Command command) {
        this.command = command;
        this.name = "Application";
        this.dataSize = 0.0;
    }

    /**
     * Initialize new custom task
     *
     * @param command Command to execute
     * @param name Name of the task
     * @param parameters
     */
    public Task(Command command, String name, TaskXMLDocument parameters) {
        this.command = command;
        this.name = name;
        this.parameters = parameters;
        this.dataSize = 0.0;
    }

    public TaskXMLDocument getParameters() {
        return parameters;
    }

    public TaskStatus getStatus() {
        return taskStatus;
    }

    public void setStatus(TaskStatus taskStatus) {
        this.taskStatus = taskStatus;
    }

    public Command getCommand() {
        return command;
    }

    @Override
    public void setDataSize(double v) {
        super.setDataSize(v);
        dataSize = v;
    }

    public Double getDataSize() {
        return dataSize;
    }


    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
