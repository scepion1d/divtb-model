package org.divtb.model.task;

/**
 * Created with IntelliJ IDEA.
 * User: scepion1d
 * Date: 3/19/13
 * Time: 1:17 PM
 * Статус выполнения задания
 */
public enum TaskStatus {
    /**
     * Задание выполняется
     */
    IN_PROGRESS,

    /**
     * Успешно выполнено
     */
    SUCCESSFUL,

    /**
     * Выполнение провалено
     */
    FAILED,

    /**
     * Выполнение остановлено
     */
    STOPPED
}
