package org.divtb.model;

import org.divtb.model.logging.LoggerFactory;
import org.simgrid.msg.Msg;
import org.simgrid.msg.MsgException;

import java.io.File;

/**
 * Main class of DiVTB model. Beginning of simulation.
 */
public class DiVTB {

    /**
     * Main method, which stats simulation.
     *
     * @param args Model configuration files (0 - platform, 1 - application)
     */
    public static void main(String[] args) throws MsgException {
        String platform = null;
        String application = null;

        // check platform and application config
        if (args.length == 2) {
            platform = checkFile(args[0]);
            application = checkFile(args[1]);
        } else {
            LoggerFactory.getLogger().fatal("Wrong argument count!" +
                    " Usage: <model.jar> /path/to/platform.xml /path/to/application.xml");
            System.exit(1);
        }

        Msg.init(new String[]{platform, application}); // initialize the MSG simulation

//      build the platform and deploy  application
        Msg.createEnvironment(platform);
        Msg.deployApplication(application);


        Msg.run(); // start simulation
    }

    /**
     * Check file existence
     *
     * @param filePath Path to file
     * @return Absolute path to file or null if file don't exists
     */
    private static String checkFile(String filePath) {
        File cfg = new File(filePath);
        if (cfg.exists())
            return cfg.getAbsolutePath();

        LoggerFactory.getLogger().fatal("Config (" + filePath + ") not found!");
        System.exit(1);
        return null;
    }

}