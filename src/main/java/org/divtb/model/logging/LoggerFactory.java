package org.divtb.model.logging;

import org.apache.log4j.Logger;

/**
 * Private class loggers factory
 */
public class LoggerFactory {

    /**
     * Generate private logger for caller of the method
     * @return Private caller logger
     */
    public static Logger getLogger() {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        // [0] is Thread, [1] is LoggerFactory, [2] is "Caller"
        String callerName = stackTrace[2].getClassName();

        return Logger.getLogger(callerName);
    }

}
