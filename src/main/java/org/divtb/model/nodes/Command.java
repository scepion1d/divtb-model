package org.divtb.model.nodes;

/**
 * Действие, которое надо выполнить по заданию
 */
public enum Command {
    /**
     * Отключить (команда узлу)
     */
    SHUT_DOWN("SHUT_DOWN"),

    /**
     * Рассчитать
     */
    CALCULATE("CALCULATE"),

    REPORT("REPORT"),

    PlAN("PLAN");

    /**
     * Строковое значение команды
     */
    protected String stringValue;


    /**
     * Инициация команды строковым значением
     * @param stringValue Строковое значение
     */
    private Command(String stringValue) {
        this.stringValue = stringValue;
    }

    /**
     * Getting enum value by string value
     * @param stringValue some string value
     * @return enum value of parameter or rise runtime exception if enum value not found
     */
    public static Command getAction(String stringValue) {
        for (Command command : Command.values()) {
            if (command.getStringValue().equals(stringValue )) {
                return command;
            }
        }
        throw new RuntimeException("Unknown parameter name \"" + stringValue + "\"");
    }

    /**
     * Getting string value of enum
     * @return string value
     */
    public String getStringValue() {
        return stringValue;
    }
}
