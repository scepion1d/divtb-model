package org.divtb.model.nodes.server;

import org.divtb.model.logging.LoggerFactory;
import org.divtb.model.nodes.Application;
import org.divtb.model.nodes.ApplicationsRegistry;
import org.divtb.model.nodes.Command;
import org.divtb.model.nodes.broker.Broker;
import org.divtb.model.task.Task;
import org.divtb.model.task.TaskStatus;
import org.simgrid.msg.Host;
import org.simgrid.msg.Msg;
import org.simgrid.msg.MsgException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: scepion1d
 * Date: 3/16/13
 * Time: 12:23 AM
 * Main class of DiVTB server
 */
public class Server extends Application {
    private int brokerPid;

    public Server(Host host, String name, String[] args) {
        super(host, name, args);
        brokerPid = -1;
        lookingForRequiredApps();
    }

    /**
     * Looking for applications required for this app
     */
    private void lookingForRequiredApps() {
        try {
            brokerPid = ApplicationsRegistry.getInstance()
                    .getAppsByName(Broker.class.getName())
                    .get(0).getPID();

            if (brokerPid == -1) {
                throw new NullPointerException("Can't find application \"Broker\"");
            }
        }
        catch (Exception e) {
            Msg.error(String.format("Can't find application: \"Broker\""));
            LoggerFactory.getLogger().fatal(e);

            System.exit(1);
        }
    }

    @Override
    public void main(String[] hosts) throws MsgException {
        Msg.info("Started");

        while (true) {
            Task task = receiveTask();

            if (task.getCommand() == Command.CALCULATE) {
                task.setStatus(TaskStatus.IN_PROGRESS);
                sendTask(execute(task), task.getSender().getPID());
            }
            else if (task.getCommand() == Command.SHUT_DOWN) {
                break;
            }
            else {
                Msg.error(String.format("Don't know what is it! \nCommand: %s\nSender: %s",
                        task.getCommand(), task.getSender().msgName()));
            }
        }

        Msg.info("Stopped");
    }

    private Task execute(Task task) {
        List<Map<Task, Integer>> plan = prepareExecutionPlan(task);
        if (plan == null) {
            task.setStatus(TaskStatus.FAILED);
            return task;
        }

        for (Map<Task, Integer> stage : plan) {
            for (Map.Entry<Task, Integer> subTask : stage.entrySet()) {
                asyncSendTask(subTask.getKey(), subTask.getValue());
            }
        }

        for (Map<Task, Integer> stage : plan) {
            for (Map.Entry<Task, Integer> subTask : stage.entrySet()) {
                receiveTask();
            }
        }

        task.setStatus(TaskStatus.SUCCESSFUL);

        return task;
    }


    private List<Map<Task, Integer>> prepareExecutionPlan(Task srcTask) {
        List<Map<Task, Integer>> plan;
        Msg.info(String.format("Preparing plan for task \"%s\".", srcTask.getName()));

        Task planingTask = new Task(Command.PlAN, srcTask.getName(), srcTask.getParameters());
        if (sendTask(planingTask, brokerPid)) { planingTask = receiveTask(); }

        try {
            if (planingTask.getStatus() == TaskStatus.SUCCESSFUL) {
                plan = (List<Map<Task, Integer>>) planingTask.getData();
            } else {
                throw new Exception("Execution failed!!");
            }

        } catch (Exception e) {
            Msg.warn(String.format("Something gone wrong with planing! Skipping task."));
            LoggerFactory.getLogger().fatal(e);

            return null;
        }

        return plan;
    }

}
