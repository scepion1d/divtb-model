package org.divtb.model.nodes;

import org.divtb.model.logging.LoggerFactory;
import org.simgrid.msg.Host;
import org.simgrid.msg.Msg;
import org.simgrid.msg.Process;
import org.divtb.model.task.Task;
import org.simgrid.msg.ProcessNotFoundException;

/**
 * Base class which describe any application in the model
 */
public abstract class Application extends Process {

    private ApplicationState state;

    protected Application(Host host, String name, String[] args) {
        super(host, name, args);
        ApplicationsRegistry.getInstance().addApplication(this);
        state = ApplicationState.AVAILABLE;
    }

    /**
     * Send task to other application
     * @param task Specific task
     * @param receiverPID Process ID of the target application
     * @return TRUE if the task sent successfully
     */
    protected boolean sendTask(Task task, int receiverPID) {
        Application application;

        try {
            application = ApplicationsRegistry.getInstance().getAppByPID(receiverPID);

            if (application != null) {
                task.send(String.valueOf(receiverPID));
            }
            else {
                throw new ProcessNotFoundException();
            }

        } catch (Exception e) {
            Msg.error(String.format("Can't send task to \"%s\".", String.valueOf(receiverPID)));
            LoggerFactory.getLogger().error(e);
            return false;
        }

        Msg.info(String.format("Task \"%s.%s\" was successfully sent to \"%s\"",
                 task.getCommand(), task.getName(), application.msgName()));
        return true;
    }

    /**
     * Async send task to other application
     * @param task Specific task
     * @param receiverPID Process ID of the target application
     * @return TRUE of the task sent successfully
     */
    protected boolean asyncSendTask(Task task, int receiverPID) {
        Application application;

        try {
            application = ApplicationsRegistry.getInstance().getAppByPID(receiverPID);

            if (application != null) {
                task.dsend(String.valueOf(receiverPID));
            }
            else {
                throw new ProcessNotFoundException();
            }

        } catch (Exception e) {
            Msg.error(String.format("Can't send task to \"%s\".", String.valueOf(receiverPID)));
            LoggerFactory.getLogger().error(e);
            return false;
        }

        Msg.info(String.format("Task \"%s.%s\" was successfully sent to \"%s\"",
                task.getCommand(), task.getName(), application.msgName()));
        return true;
    }

    /**
     * Receive a task from other application
     * @return Task from other application or NULL
     */
    protected Task receiveTask() {
        Task task;

        try {
            task = (Task) Task.receive(String.valueOf(this.pid));
        }
        catch (Exception e) {
            Msg.error("Can't receive task.");
            LoggerFactory.getLogger().error(e);

            return null;
        }

        Msg.info(String.format("Task \"%s.%s\" was successfully received from \"%s\"",
                 task.getCommand(), task.getName(), task.getSender().msgName()));
        return task;
    }

    public ApplicationState getState() {
        return state;
    }

    public void setState(ApplicationState state) {
        this.state = state;
    }
}
