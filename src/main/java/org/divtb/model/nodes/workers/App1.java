package org.divtb.model.nodes.workers;

import org.divtb.model.nodes.Application;
import org.divtb.model.nodes.Command;
import org.divtb.model.task.Task;
import org.divtb.model.task.TaskStatus;
import org.simgrid.msg.Host;
import org.simgrid.msg.Msg;
import org.simgrid.msg.MsgException;

/**
 * Created with IntelliJ IDEA.
 * User: scepion1d
 * Date: 3/17/13
 * Time: 12:10 PM
 * Main class of the DiVTB model workers nodes
 */
public class App1 extends Application {

    public App1(Host host, String name, String[] strings) {
        super(host, name, strings);
    }

    @Override
    public void main(String[] strings) throws MsgException {
        Msg.info("Started");

        while (true) {
            Task task = receiveTask();

            if (task.getCommand() == Command.CALCULATE) {
                waitFor(10000);
                task.setStatus(TaskStatus.SUCCESSFUL);
                sendTask(task, task.getSender().getPID());
            }
            else if (task.getCommand() == Command.SHUT_DOWN) {
                break;
            }
            else {
                Msg.error(String.format("Don't know what is it! \nCommand: %s\nSender: %s",
                        task.getCommand(), task.getSender().msgName()));
            }
        }

        Msg.info("Stopped");
    }
}
