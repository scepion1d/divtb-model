package org.divtb.model.nodes;

/**
 * State of the processor
 */
public enum ApplicationState {
    /**
     * Ready fot action
     */
    AVAILABLE,

    /**
     * Busy by executions of a task
     */
    BUSY,

    /**
     * Disabled
     */
    DISABLED,

    /**
     * Reserved by broker for execution of a task
     */
    RESERVED
}
