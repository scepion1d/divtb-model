package org.divtb.model.nodes;

import org.divtb.model.logging.LoggerFactory;
import org.simgrid.msg.Host;
import org.simgrid.msg.HostNotFoundException;
import org.simgrid.msg.Msg;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Registry of all applications and their applications
 */
public class ApplicationsRegistry {
    /**
     * Registry of all applications of the system
     * Key: Application
     * Value: State of the application
     */
    private Vector<Application> registry;

    private static final ApplicationsRegistry instance;

    static {
        instance = new ApplicationsRegistry();
    }

    public static ApplicationsRegistry getInstance() {
        return instance;
    }

    private ApplicationsRegistry() {
        registry = new Vector<>();
    }

    public void addApplication(Application application) {
        registry.add(application);
    }

    public List<String> getAllHostsNames() {
        List<String> hostsNames = new ArrayList<>();

        for (Application application : registry) {
            hostsNames.add(application.getHost().getName());
        }

        return hostsNames;
    }

    public List<Application> getAppsOfHost(String hostName) {
        List<Application> applications = new ArrayList<>();

        try {
            Host.getByName(hostName);

            for (Application application : registry) {
                if (application.getHost().getName().equals(hostName)) {
                    applications.add(application);
                }
            }
        } catch (HostNotFoundException e) {
            Msg.error("Host with name \"" + hostName + "\" not found.");
            LoggerFactory.getLogger().error(e);
            return null;
        }

        return applications;
    }


    public List<Application> getAppsByName(String appName) {
        List<Application> applications = new ArrayList<>();

        for (Application application : registry) {
            if (application.msgName().equals(appName)) {
                applications.add(application);
            }
        }

        return applications;
    }

    public Application getAppByPID(int PID) {
        for (Application application : registry) {
            if (application.getPID() == PID) {
                return application;
            }
        }

        return null;
    }


    public List<Application> getAll() {
        return new ArrayList<>(registry);
    }

    public List<Application> getAppsByNameAndState(String appName, ApplicationState state) {
        List<Application> applications = new ArrayList<>();

        for (Application application : registry) {

            if (application.msgName().equals(appName) && application.getState() == state) {
                applications.add(application);
            }
        }

        return applications;
    }

}
