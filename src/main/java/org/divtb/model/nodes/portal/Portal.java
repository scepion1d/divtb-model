package org.divtb.model.nodes.portal;

import org.divtb.model.logging.LoggerFactory;
import org.divtb.model.xml.TaskXMLDocument;
import org.divtb.model.nodes.Application;
import org.divtb.model.nodes.ApplicationsRegistry;
import org.divtb.model.nodes.server.Server;
import org.divtb.model.nodes.Command;
import org.divtb.model.task.Task;
import org.simgrid.msg.Host;
import org.simgrid.msg.Msg;
import org.simgrid.msg.MsgException;
import org.simgrid.msg.ProcessNotFoundException;

/**
 * Main class of DiVTB model portal nodes
 */
public class Portal extends Application {
    private int serverPid;

    public Portal(Host host, String name, String[] args) {
        super(host, name, args);
        serverPid = -1;
        lookingForRequiredApps();
    }

    /**
     * Looking for applications required for this app
     */
    private void lookingForRequiredApps() {
        try {
            serverPid = ApplicationsRegistry.getInstance()
                    .getAppsByName(Server.class.getName())
                    .get(0).getPID();

            if (serverPid == -1) {
                throw new ProcessNotFoundException("Can't find application \"Server\"");
            }
        } catch (Exception e) {
            Msg.error(String.format("Can't find application: \"Server\""));
            LoggerFactory.getLogger().fatal(e);

            System.exit(1);
        }
    }

    @Override
    public void main(String[] args) throws MsgException {
        Msg.info("Started");

        if (args.length != 0) // Если есть задания
            for (String arg : args) {
                Task task = buildTask(arg);
                Task resultTask = null;

                if (sendTask(task, serverPid)) {
                    resultTask = receiveTask();
                    Msg.info(String.format("Task \"%s.%s\" finished with the status \"%s\".",
                            resultTask.getCommand(), resultTask.getName(), resultTask.getStatus()));
                }
            }

        stopSystem();
        Msg.info("Stopped");
    }

    /**
     * Create new task object from xml configuration
     *
     * @param xmlConfigPath Путь до конфигурационного файла
     * @return Задание
     */
    private Task buildTask(String xmlConfigPath) {
        TaskXMLDocument cfg;

        try {
            cfg = new TaskXMLDocument(xmlConfigPath, true);
        } catch (Exception e) {
            Msg.error(String.format("Can't parse task configuration \"%s\". This task will be skipped.",
                      xmlConfigPath));
            LoggerFactory.getLogger().error(e);

            return null;
        }

        return new Task(Command.getAction(cfg.getTagValue("action")), cfg.getTagValue("name"), cfg);
    }

    /**
     * Stop all nodes of the model
     */
    private void stopSystem() {
        for (Application app : ApplicationsRegistry.getInstance().getAll()) {
            if (app.getPID() != pid) {
                sendTask(new Task(Command.SHUT_DOWN), app.getPID());
            }
        }
    }

}
