package org.divtb.model.nodes.broker;

import org.divtb.model.nodes.Application;
import org.divtb.model.nodes.Command;
import org.divtb.model.task.Task;
import org.divtb.model.task.TaskStatus;
import org.simgrid.msg.Host;
import org.simgrid.msg.Msg;
import org.simgrid.msg.MsgException;
import org.simgrid.msg.ProcessNotFoundException;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: scepion1d
 * Date: 3/17/13
 * Time: 12:10 PM
 * Main class of the DiVTB model broker node
 */
public class Broker extends Application {

    private Selector selector;

    public Broker(Host host, String name, String[] args) {
        super(host, name, args);
        selector = new FirstAvailable();
    }

    @Override
    public void main(String[] strings) throws MsgException {
        Msg.info("Started");

        while (true) {
            Task task = receiveTask();

            if (task.getCommand() == Command.PlAN) {
                task.setStatus(TaskStatus.IN_PROGRESS);
                sendTask(execute(task), task.getSender().getPID());
            } else if (task.getCommand() == Command.SHUT_DOWN) {
                break;
            } else {
                Msg.error(String.format("Don't know what is it! \nCommand: %s\nSender: %s",
                        task.getCommand(), task.getSender().msgName()));
            }
        }

        Msg.info("Stopped");
    }

    private Task execute(Task task) {
        Integer PID = null;

        for (List<String> stage : task.getParameters().getSubTasksApps()) {
            Map<Task, Integer> stagePlan = new HashMap<>();

            for (String appName : stage) {
                try {
                    int requiredPID = selector.getRequiredApplicationPID(appName);
                    Task subTask = new Task(Command.CALCULATE, appName, null);

                    stagePlan.put(subTask, requiredPID);
                }
                catch (ProcessNotFoundException e) {
                    Msg.error(
                            String.format("Unknown application \"%s\"! Planning of the task \"%s\" stopped!", appName, task.getName()));
                    task.setStatus(TaskStatus.FAILED);
                    return task;
                }
            }

            plan.add(stagePlan);
        }
        task.setStatus(TaskStatus.SUCCESSFUL);
        task.setData(PID);
        return task;
    }

//    private Task execute(Task task) {
//        List<Map<Task, Integer>> plan = new ArrayList<>();
//
//        for (List<String> stage : task.getParameters().getSubTasksApps()) {
//            Map<Task, Integer> stagePlan = new HashMap<>();
//
//            for (String appName : stage) {
//                try {
//                    int requiredPID = selector.getRequiredApplicationPID(appName);
//                    Task subTask = new Task(Command.CALCULATE, appName, null);
//
//                    stagePlan.put(subTask, requiredPID);
//                }
//                catch (ProcessNotFoundException e) {
//                    Msg.error(
//                            String.format("Unknown application \"%s\"! Planning of the task \"%s\" stopped!", appName, task.getName()));
//                    task.setStatus(TaskStatus.FAILED);
//                    return task;
//                }
//            }
//
//            plan.add(stagePlan);
//        }
//        task.setStatus(TaskStatus.SUCCESSFUL);
//        task.setData(plan);
//        return task;
//    }

}
