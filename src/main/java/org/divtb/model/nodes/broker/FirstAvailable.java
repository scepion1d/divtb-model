package org.divtb.model.nodes.broker;

import org.divtb.model.nodes.Application;
import org.divtb.model.nodes.ApplicationState;
import org.divtb.model.nodes.ApplicationsRegistry;
import org.simgrid.msg.ProcessNotFoundException;

import java.util.List;

/**
 * Class for getting first available application
 */
public class FirstAvailable implements Selector {

    @Override
    public int getRequiredApplicationPID(String appName) throws ProcessNotFoundException {
        List<Application> apps =
                ApplicationsRegistry.getInstance()
                        .getAppsByNameAndState(appName, ApplicationState.AVAILABLE);

        if (apps == null || apps.isEmpty()) {
            apps = ApplicationsRegistry.getInstance()
                    .getAppsByNameAndState(appName, ApplicationState.RESERVED);
        }

        if (apps == null || appName.isEmpty()) {
            apps = ApplicationsRegistry.getInstance()
                    .getAppsByNameAndState(appName, ApplicationState.BUSY);
        }

        if (apps == null || apps.isEmpty()) {
            throw new ProcessNotFoundException(
                    String.format("Process with name \"%s\" not found", appName));
        }

        if (apps.get(0).getState() != ApplicationState.BUSY) {
            apps.get(0).setState(ApplicationState.RESERVED);
        }

        return apps.get(0).getPID();
    }

}
