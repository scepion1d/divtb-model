package org.divtb.model.nodes.broker;

import org.simgrid.msg.ProcessNotFoundException;

/**
 * Interface for the logic specific logic of the node selection
 */
public interface Selector {

    /**
     * Get pid of the required application
     *
     * @param appName Name of the required application
     * @return PID of the required application
     * @throws ProcessNotFoundException If the application doesn't exists
     */
    public int getRequiredApplicationPID(String appName) throws ProcessNotFoundException;

}
