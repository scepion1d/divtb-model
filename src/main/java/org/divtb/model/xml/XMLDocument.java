package org.divtb.model.xml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Base XML document parser
 */
public class XMLDocument {
    protected Document doc;

    /**
     * Initialize new xml document
     * @param path Path to the .xml file
     * @param skipValidation Do you want to skip xml validation with dtd schema?
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     */
    public XMLDocument(String path, boolean skipValidation) throws IOException, SAXException, ParserConfigurationException {
        File fXmlFile = new File(path);
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        if (skipValidation) {
            dbf.setValidating(false);
        }

        DocumentBuilder db = dbf.newDocumentBuilder();
        if (skipValidation) {
            db.setEntityResolver(new EntityResolver() {
                @Override
                public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
                    if (systemId.contains(".dtd")) {
                        return new InputSource(new StringReader(""));
                    } else {
                        return null;
                    }
                }
            });
        }

        doc = db.parse(fXmlFile);
        doc.getDocumentElement().normalize();
    }

    /**
     * Gat values of specific attributes from all entry of specific tag
     *
     * @param tagName        Name of the tag
     * @param attributeNames Name of the specific attribute
     * @return List of attribute values
     */
    public List<Map<String, String>> getAllValues(String tagName, List<String> attributeNames) {
        List<Map<String, String>> tags = new ArrayList<>();
        NodeList nodes = doc.getElementsByTagName(tagName);

        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Map<String, String> attrValues = new HashMap<>();

                for (String attributeName : attributeNames) {
                    attrValues.put(attributeName,
                            ((Element) node).getAttribute(attributeName));
                }

                tags.add(attrValues);
            }
        }

        return tags;
    }

    /**
     * Get value of the first entry of the specific tag
     *
     * @param tagName Name of the tag
     * @return Value of the tag
     */
    public String getTagValue(String tagName) {
        NodeList nodes = doc.getElementsByTagName(tagName);
        if (nodes.getLength() > 0) {
            return nodes.item(0).getTextContent();
        } else {
            return null;
        }
    }

    /**
     * \
     * Get values of all entries of the specific tag
     *
     * @param tagName Name of the tag
     * @return List of the tag values
     */
    public List<String> getTagValues(String tagName) {
        List<String> values = new ArrayList<>();
        NodeList nodes = doc.getElementsByTagName(tagName);

        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                values.add(node.getTextContent());
            }
        }

        return values;
    }
}
