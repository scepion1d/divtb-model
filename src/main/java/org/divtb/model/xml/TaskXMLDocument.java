package org.divtb.model.xml;

import org.simgrid.msg.Msg;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * XML Parser for task configuration
 */
public class TaskXMLDocument extends XMLDocument {

    /**
     * Initialize new  task xml document
     *
     * @param path           Path to the .xml file
     * @param skipValidation Do you want to skip xml validation with dtd schema?
     * @throws java.io.IOException
     * @throws org.xml.sax.SAXException
     * @throws javax.xml.parsers.ParserConfigurationException
     *
     */
    public TaskXMLDocument(String path, boolean skipValidation) throws IOException, SAXException, ParserConfigurationException {
        super(path, skipValidation);
    }

    /**
     * Get list of sub tasks divided into stages
     *
     * @return List of stages looks like list of sub tasks
     */
    public List<List<String>> getSubTasksApps() {
        List<List<String>> result = new ArrayList<>();

        // get all stages
        NodeList stages = doc.getElementsByTagName("stage");
        for (int i = 0; i < stages.getLength(); i++) {
            Element stage = (Element) stages.item(i);

            if (stage != null && stage.getNodeType() == Node.ELEMENT_NODE) {
                List<String> stageSubTasks = new ArrayList<>();

                // get subtasks of the stage
                NodeList subTasks = stage.getElementsByTagName("subtask");
                for (int j = 0; j < subTasks.getLength(); j++) {
                    Node subTask = subTasks.item(j);

                    if (subTask != null && subTask.getNodeType() == Node.ELEMENT_NODE) {
                        String appName = subTask.getTextContent();

                        if (!appName.replaceAll("\\s", "").isEmpty())
                            stageSubTasks.add(appName.replaceAll("\\s", ""));
                    }
                }

                result.add(stageSubTasks);
            }
        }

        return result;
    }
}
